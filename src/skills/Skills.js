import { Box, Typography } from "@material-ui/core"
import React from "react"
// import { useStyles } from "../componnets/css/styles.js";
import { skills } from '../componnets/util/Util.js';
import "./Skills.css"
import TechCard from "./TechCard.js";

export default function Skills() {
    // const classes = useStyles()
    return (
        <>
            <Typography variant="h6" gutterBottom >Technical Skills</Typography>
            {/* <Box
                display="flex"
                flexWrap="wrap"
                css={{ maxWidth: "100%" }}
            >
                <Box m={2} p={2} css={{ width: "200px" }}>
                    <Typography variant="body1" className="skillValue">{skills[0].name}</Typography>
                    <LinearProgress variant="determinate" value={skills[0].value} classes={{
                        colorPrimary: classes.colorPrimary0,
                        barColorPrimary: classes.barColorPrimary0
                    }} />
                </Box>
                <Box m={2} p={2} css={{ width: "200px" }}>
                    <Typography variant="body1" className="skillValue">{skills[1].name}</Typography>
                    <LinearProgress variant="determinate" value={skills[1].value} classes={{
                        colorPrimary: classes.colorPrimary1,
                        barColorPrimary: classes.barColorPrimary1
                    }} />
                </Box>
                <Box m={2} p={2} css={{ width: "200px" }}>
                    <Typography variant="body1" className="skillValue">{skills[2].name}</Typography>
                    <LinearProgress variant="determinate" value={skills[2].value} classes={{
                        colorPrimary: classes.colorPrimary2,
                        barColorPrimary: classes.barColorPrimary2
                    }} />
                </Box>
                <Box m={2} p={2} css={{ width: "200px" }}>
                    <Typography variant="body1" className="skillValue">{skills[3].name}</Typography>
                    <LinearProgress variant="determinate" value={skills[3].value} classes={{
                        colorPrimary: classes.colorPrimary3,
                        barColorPrimary: classes.barColorPrimary3
                    }} />
                </Box>
                <Box m={2} p={2} css={{ width: "200px" }}>
                    <Typography variant="body1" className="skillValue">{skills[4].name}</Typography>
                    <LinearProgress variant="determinate" value={skills[4].value} classes={{
                        colorPrimary: classes.colorPrimary4,
                        barColorPrimary: classes.barColorPrimary4
                    }} />
                </Box>
                <Box m={2} p={2} css={{ width: "200px" }}>
                    <Typography variant="body1" className="skillValue">{skills[5].name}</Typography>
                    <LinearProgress variant="determinate" value={skills[5].value} classes={{
                        colorPrimary: classes.colorPrimary5,
                        barColorPrimary: classes.barColorPrimary5
                    }} />
                </Box>
                <Box m={2} p={2} css={{ width: "200px" }}>
                    <Typography variant="body1" className="skillValue">{skills[6].name}</Typography>
                    <LinearProgress variant="determinate" value={skills[6].value} classes={{
                        colorPrimary: classes.colorPrimary6,
                        barColorPrimary: classes.barColorPrimary6
                    }} />
                </Box>

            </Box> */}
            <Box
                display="flex"
                flexWrap="wrap"
                css={{ maxWidth: "100%" }}
            >
                {skills.map((item,index)=>(
                    <TechCard skill={item}/>
                ))}
            </Box>
        </>
    )
}