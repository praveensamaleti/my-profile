import { AppBar, Box, Button, Hidden, makeStyles, Toolbar } from "@material-ui/core";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { p1validated, updateStep } from "../../redux/actions";

const useStyles = makeStyles((theme) => ({
    appBar: {
        top: 'auto',
        bottom: 0,
    },
    colorPrimary: {
        backgroundColor: "white",
        top: "auto",
        bottom: "0"
    }
}))
function Footer() {
    const classes = useStyles();
    const currentStep = useSelector(state => state.currentStep);
    const dispatch = useDispatch();
    function handleNext(){
        console.log(" handle next")
        let validated = true;
        // if(currentStep === "Page1" ){
            if(validated){
                console.log("in footer handle next")
                dispatch(p1validated(true))
                dispatch(updateStep("Page2"))
            }
        // }
    }
    function handlePrevious(){
        if(currentStep === "Page2"){
            dispatch(p1validated(false))
            dispatch(updateStep("Page1"))
        }
    }
    return (
        <AppBar position="fixed" classes={{ colorPrimary: classes.colorPrimary }}>
            <Toolbar >
                <Box display="flex" flexDirection="row">
                    <Box p={1}>
                        <Hidden xsDown implementation="css">
                            <div style={{ minWidth: 240 }}></div>
                        </Hidden>
                    </Box>
                    <Box p={1} flexDirection="row" justifyContent="space-between" display="flex" style={{ minWidth: "80%" }}>
                        {currentStep !== "Page1" && <Button variant="contained" onClick={handlePrevious}>Previous</Button>}
                        <Button variant="contained" style={{marginLeft: "auto"}} onClick={handleNext}>Next</Button>
                    </Box>
                </Box>
            </Toolbar>
        </AppBar>
    )
}

export default Footer;