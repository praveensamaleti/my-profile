import React from 'react';
import { VerticalTimeline, VerticalTimelineElement } from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';


export default function Experience() {
  return (
    <VerticalTimeline className="exp-timeline" layout="1-column-left" animate={false}>
      <VerticalTimelineElement
        className="vertical-timeline-element--work"
        contentStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
        contentArrowStyle={{ borderRight: '7px solid  rgb(33, 150, 243)' }}
        date="2019 - present"
        position="right"
      >
        <h3 className="vertical-timeline-element-title">IT Analyst</h3>
        <h4 className="vertical-timeline-element-subtitle">Tata Consultancy Services</h4>
        <p>
          Roles and ResponsibilitiesRoles and ResponsibilitiesRoles and ResponsibilitiesRoles and Responsibilities
        </p>
      </VerticalTimelineElement>
      <VerticalTimelineElement
        className="vertical-timeline-element--work"
        date="2017 - 2019"
        position="right"
      >
        <h3 className="vertical-timeline-element-title">Systems Engineer</h3>
        <h4 className="vertical-timeline-element-subtitle">Tata Consultancy Services</h4>
        <p>
          Roles and Responsibilities
        </p>
      </VerticalTimelineElement>
      <VerticalTimelineElement
        className="vertical-timeline-element--work"
        date="2016 - 2017"
        position="right"
      >
        <h3 className="vertical-timeline-element-title">Application Development Analyst</h3>
        <h4 className="vertical-timeline-element-subtitle">Accenture Services Private Limited</h4>
        <p>
          Roles and Responsibilities
        </p>
      </VerticalTimelineElement>
      <VerticalTimelineElement
        className="vertical-timeline-element--work"
        date="2014 - 2016"
        position="right"
      >
        <h3 className="vertical-timeline-element-title">Application Development Analyst Trainee</h3>
        <h4 className="vertical-timeline-element-subtitle">Accenture Services Private Limited</h4>
        <p>
          Roles and Responsibilities
        </p>
      </VerticalTimelineElement>
    </VerticalTimeline>
  );
}
