
const MenuList = [
    // {
    //     id: 0,
    //     pathTo: "/home",
    //     name: "HOME"
    // },
    // {
    //     id: 1,
    //     pathTo: "/about",
    //     name: "ABOUT"
    // },
    // {
    //     id: 2,
    //     pathTo: "/skills",
    //     name: "SKILLS"
    // },
    // {
    //     id: 3,
    //     pathTo: "/education",
    //     name: "EDUCATION"
    // },
    // {
    //     id: 4,
    //     pathTo: "/experience",
    //     name: "EXPERIENCE"
    // },
    // {
    //     id: 5,
    //     pathTo: "/contact",
    //     name: "CONTACT"
    // },
    {
        id: 6,
        pathTo: "/create",
        name: "CREATE"
    },
    {
        id:7,
        pathTo:"/anime",
        name: "ANIME"
    },
    {
        id:8,
        pathTo:"/date",
        name: "DATE"
    }
]

const skills = [
    {
        id: 0,
        name: "Core Java",
        value: 80,
        relevantExp: "7 years",
        cp: "rgb(182, 188, 226)",
        bcp: "#3f51b5"
    },
    {
        id: 1,
        name: "JavaScript",
        value: 80,
        relevantExp: "4 years",
        cp: "",
        bcp: ""
    },
    {
        id: 2,
        name: "RESTful web services",
        value: 65,
        relevantExp: "6 years",
        cp: "",
        bcp: ""
    },
    {
        id: 3,
        name: "JPA",
        value: 75,
        relevantExp: "3 years",
        cp: "",
        bcp: ""
    },
    {
        id: 4,
        name: "Hibernate",
        value: 70,
        relevantExp: "3 years",
        cp: "",
        bcp: ""
    },
    {
        id: 5,
        name: "Spring Boot",
        value: 80,
        relevantExp: 3,
        cp: "",
        bcp: ""
    },
    {
        id: 6,
        name: "ReactJs",
        value: 85,
        relevantExp: "4 years",
        cp: "",
        bcp: ""
    }
]

const summary = [
    "7 years of experience in Backend Application Development, Web Development, Designing User Interface Applications, Testing and documentation of web-based applications using Java, J2EE technologies, RESTful Web Services, JPA, Hibernate, Spring Boot, Kafka, Spring State Machine, HTML5, CSS, JavaScript, ReactJs and Redux.",
    "Experience in Design and Development of Object-Oriented Software Systems and building complex, high performance, scalable and easily maintainable solutions for Web and distributed applications.",
    "Experienced in developing UML Diagrams like use cases, Class Diagrams and Sequence Diagrams.",
    "Experience in developing Web Applications with ReactJs, Redux, Material UI and Bootstrap.",
    "Extensive Experience in IDE and debugging tools like Eclipse, IntelliJ, WebStorm and VSCode.",
    "Experience working on Application servers and Web servers like IBM WebSphere, JBOSS and Apache Tomcat.",
    "Experience in working Agile Scrum methodology.",
    "Experience in Developing Event driven architecture using Spring Kafka, Spring State Machine, Spring Boot.",
    "Performed unit testing using JUnit aiding test driven Development in some scenarios.",
    "Integration and deployment of applications done using tools like SVN and GIT.",
    "Highly adaptable in quickly changing technical environments with very strong organizational and analytical skills"
]
export { MenuList, skills, summary }