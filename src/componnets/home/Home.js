import React from "react"
import { summary } from "../util/Util"
import { Typography } from "@material-ui/core"
import Button from '@material-ui/core/Button';

export default function Home() {
    return (
        <>
            <Typography variant="h6" gutterBottom >About Me</Typography>
            <ul>
                {summary.map((item, index) => (
                    <li>
                        <Typography>{item}</Typography>
                    </li>
                ))}
            </ul>
            <Button variant="outlined" style={{marginTop:30}}><a href="/Resume.pdf" style={{textDecoration: "none",color: "Black"}} download>Download Resume</a></Button>
        </>
    )
}