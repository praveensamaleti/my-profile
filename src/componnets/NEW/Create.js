import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Footer from "../footer/Footer";
import Page1 from "./Page1";
import Page2 from "./Page2";

function Create(){
    const newStep = useSelector(state => state.currentStep);
    const [currentStep,setCurrentStep] = useState("Page1")
    // const state = useSelector(state => state);
    // const dispatch = useDispatch();
    useEffect(()=>{
        console.log("updated in useEffect",newStep,currentStep)
        newStep && setCurrentStep(newStep)
    },[newStep,currentStep])
    return(
        <>
            {currentStep === "Page1" && <Page1/>}
            {currentStep === "Page2" && <Page2/>}
            <Footer/>
        </>
    )
}

export default Create