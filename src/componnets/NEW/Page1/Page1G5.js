import { Box, Grid, Paper } from "@material-ui/core";

function Page1G5() {
    return (
        <Grid container spacing={3} style={{ padding: "10px 0px 20px 0px" }}>
            <Grid item xs={5}>
                <Paper style={{ padding: "20px" }}>
                    <Box p={1}>Box Header</Box>
                    <Box p={1}>Box Sub Header</Box>
                    <Box p={1}>Box Content1</Box>
                    <Box p={1}>Box Content2</Box>
                    <Box p={1}>Box Content3</Box>
                    <Box p={1}>Box Content14</Box>
                </Paper>
            </Grid>
            <Grid item xs={2}>

            </Grid>
            <Grid item xs={5}>
                <Paper style={{ padding: "20px" }}>
                    <Box p={1}>Box Header</Box>
                    <Box p={1}>Box Sub Header</Box>
                    <Box p={1}>Box Content1</Box>
                    <Box p={1}>Box Content2</Box>
                    <Box p={1}>Box Content3</Box>
                    <Box p={1}>Box Content14</Box>
                </Paper>
            </Grid>
        </Grid>
    )
}

export default Page1G5;