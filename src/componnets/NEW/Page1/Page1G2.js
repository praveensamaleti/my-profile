import { Grid, TextField } from "@material-ui/core"
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { updatePage1 } from "../../../redux/actions";
function Page1G2(props) {
    const [field4, setField4] = useState();
    const [field5, setField5] = useState();
    const [field6, setField6] = useState();
    const dispatch = useDispatch();
    
    useEffect(() => {
        props.p1Validated && dispatch(updatePage1({ field4, field5, field6 }))
        if (props.page1Values) {
            setField4(props.page1Values.field4)
            setField5(props.page1Values.field5)
            setField6(props.page1Values.field6)
        }

    }, [props.p1Validated,props.page1Values,dispatch,field4,field5,field6])
    function handleChange(e) {
        if (e.target.name === "field4") {
            setField4(e.target.value)
        } else if (e.target.name === "field5") {
            setField5(e.target.value)
        } else if (e.target.name === "field6") {
            setField6(e.target.value)
        }
    }
    return (
        <Grid container spacing={3} style={{ padding: "10px 0px 20px 0px" }}>
            <Grid item xs={4}>
                <TextField id="outlined-basic" label="field4" name="field4" variant="outlined" value={field4} onChange={handleChange} />
            </Grid>
            <Grid item xs={4}>
                <TextField id="outlined-basic" label="field5" name="field5" variant="outlined" value={field5} onChange={handleChange} />
            </Grid>
            <Grid item xs={4}>
                <TextField id="outlined-basic" label="field6" name="field6" variant="outlined" value={field6} onChange={handleChange} />
            </Grid>
        </Grid>
    )
}
export default Page1G2;