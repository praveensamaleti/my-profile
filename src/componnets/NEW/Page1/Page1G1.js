import { Grid, TextField } from "@material-ui/core"
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { updatePage1 } from "../../../redux/actions";
function Page1G1(props) {
    const [field1, setField1] = useState();
    const [field2, setField2] = useState();
    const [field3, setField3] = useState();
    const dispatch = useDispatch();
    useEffect(() => {
        props.p1Validated && dispatch(updatePage1({ field1, field2, field3 }))
        if (props.page1Values) {
            setField1(props.page1Values.field1);
            setField2(props.page1Values.field2);
            setField3(props.page1Values.field3);
        }
    }, [props.p1Validated,props.page1Values,dispatch,field1,field2,field3])
    

    function handleChange(e) {
        if (e.target.name === "field1") {
            setField1(e.target.value)
        } else if (e.target.name === "field2") {
            setField2(e.target.value)
        } else if (e.target.name === "field3") {
            setField3(e.target.value)
        }
    }
    return (
        <Grid container spacing={3} style={{ padding: "10px 0px 20px 0px" }}>
            <Grid item xs={4}>
                <TextField id="outlined-basic" label="field1" name="field1" variant="outlined" value={field1} onChange={handleChange} />
            </Grid>
            <Grid item xs={4}>
                <TextField id="outlined-basic" label="field2" name="field2" variant="outlined" value={field2} onChange={handleChange} />
            </Grid>
            <Grid item xs={4}>
                <TextField id="outlined-basic" label="field3" name="field3" variant="outlined" value={field3} onChange={handleChange} />
            </Grid>
        </Grid>
    )
}
export default Page1G1;