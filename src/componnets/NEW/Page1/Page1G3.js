import { Grid, TextField } from "@material-ui/core"
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { updatePage1 } from "../../../redux/actions";
function Page1G3(props) {
    const [field7,setField7] = useState();
    const [field8,setField8] = useState();
    const [field9,setField9] = useState();
    const dispatch = useDispatch();
    
    useEffect(()=>{
        props.p1Validated && dispatch(updatePage1({field7,field8,field9}))
        if (props.page1Values) {
            setField7(props.page1Values.field7)
            setField8(props.page1Values.field8)
            setField9(props.page1Values.field9)
        }
    },[props.p1Validated,props.page1Values,dispatch,field7,field8,field9])
    function handleChange(e){
        if(e.target.name==="field7"){
            setField7(e.target.value)
        }else if(e.target.name==="field8"){
            setField8(e.target.value)
        }else if(e.target.name==="field9"){
            setField9(e.target.value)
        }
    }
    return (
        <Grid container spacing={3} style={{ padding: "10px 0px 20px 0px" }}>
            <Grid item xs={4}>
                <TextField id="outlined-basic" label="field7" name="field7" variant="outlined" value={field7} onChange={handleChange}/>
            </Grid>
            <Grid item xs={4}>
                <TextField id="outlined-basic" label="field8" name="field8" variant="outlined" value={field8} onChange={handleChange}/>
            </Grid>
            <Grid item xs={4}>
                <TextField id="outlined-basic" label="field9" name="field9" variant="outlined" value={field9} onChange={handleChange}/>
            </Grid>
        </Grid>
    )
}
export default Page1G3;