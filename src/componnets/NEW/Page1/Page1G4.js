import { Grid, TextField } from "@material-ui/core"
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { updatePage1 } from "../../../redux/actions";
function Page1G4(props) {
    const [field10,setField10] = useState();
    const [field11,setField11] = useState();
    const [field12,setField12] = useState();
    const dispatch = useDispatch();
    
    useEffect(()=>{
        if (props.page1Values) {
            setField10(props.page1Values.field10)
            setField11(props.page1Values.field11)
            setField12(props.page1Values.field12)
        }
        props.p1Validated && dispatch(updatePage1({field10,field11,field12}))
    },[props.p1Validated,props.page1Values,dispatch,field10,field11,field12])
    
    function handleChange(e){
        if(e.target.name==="field10"){
            setField10(e.target.value)
        }else if(e.target.name==="field11"){
            setField11(e.target.value)
        }else if(e.target.name==="field12"){
            setField12(e.target.value)
        }
    }
    return (
        <Grid container spacing={3} style={{ padding: "10px 0px 20px 0px" }}>
            <Grid item xs={4}>
                <TextField id="outlined-basic" label="field10" name="field10" variant="outlined" value={field10} onChange={handleChange}/>
            </Grid>
            <Grid item xs={4}>
                <TextField id="outlined-basic" label="field11" name="field11" variant="outlined" value={field11} onChange={handleChange}/>
            </Grid>
            <Grid item xs={4}>
                <TextField id="outlined-basic" label="field12" name="field12" variant="outlined" value={field12} onChange={handleChange}/>
            </Grid>
        </Grid>
    )
}
export default Page1G4;