import { Grid, TextField } from "@material-ui/core";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { updatePage1 } from "../../../redux/actions";
function Page1G6(props) {
    const [field13,setField13] = useState();
    const [field14,setField14] = useState();
    const [field15,setField15] = useState();
    const dispatch = useDispatch();
    
    useEffect(()=>{
        props.p1Validated && dispatch(updatePage1({field13,field14,field15}))
        if(props.page1Values){
            setField13(props.page1Values.field13)
            setField14(props.page1Values.field14)
            setField15(props.page1Values.field15)
        }
    },[props.p1Validated,props.page1Values,dispatch,field13,field14,field15])
    function handleChange(e){
        if(e.target.name==="field13"){
            setField13(e.target.value)
        }else if(e.target.name==="field14"){
            setField14(e.target.value)
        }else if(e.target.name==="field15"){
            setField15(e.target.value)
        }
    }
    return (
        <Grid container spacing={3} style={{ padding: "10px 0px 80px 0px" }}>
            <Grid item xs={4}>
                <TextField id="outlined-basic" label="field13" name="field13" variant="outlined" value={field13} onChange={handleChange}/>
            </Grid>
            <Grid item xs={4}>
                <TextField id="outlined-basic" label="field14" name="field14" variant="outlined" value={field14} onChange={handleChange}/>
            </Grid>
            <Grid item xs={4}>
                <TextField id="outlined-basic" label="field15" name="field15" variant="outlined" value={field15} onChange={handleChange}/>
            </Grid>
        </Grid>
    )
}

export default Page1G6;