import { Box, Container } from "@material-ui/core"
import { useSelector } from "react-redux"
import Page1G1 from "./Page1/Page1G1"
import Page1G2 from "./Page1/Page1G2"
import Page1G3 from "./Page1/Page1G3"
import Page1G4 from "./Page1/Page1G4"
import Page1G5 from "./Page1/Page1G5"
import Page1G6 from "./Page1/Page1G6"


function Page1(){
    const p1Validated = useSelector(state => state.p1Validated);
    const page1Values = useSelector(state => state.page1Values);
    // const dispatch = useDispatch();
    
    return(
        <Container style={{minWidth: "100%"}}>
            <Box p={2} style={{textAlign: "center"}}><b>Header</b></Box>
            <Box p={2} style={{textAlign: "center"}}><b>Sub Header</b></Box>
            <Page1G1 p1Validated={p1Validated} page1Values={page1Values}/>
            <Page1G2 p1Validated={p1Validated} page1Values={page1Values}/>
            <Page1G3 p1Validated={p1Validated} page1Values={page1Values}/>
            <Page1G4 p1Validated={p1Validated} page1Values={page1Values}/>
            <Page1G5 p1Validated={p1Validated} page1Values={page1Values}/>
            <Page1G6 p1Validated={p1Validated} page1Values={page1Values}/>
        </Container>
    )
}

export default Page1