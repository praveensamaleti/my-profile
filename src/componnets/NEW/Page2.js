import { Box, Container, Grid, Paper, TextField } from "@material-ui/core"


function Page2(){
    return(
        <Container style={{minWidth: "100%"}}>
            <Box p={2} style={{textAlign: "center"}}><b>Header</b></Box>
            <Box p={2} style={{textAlign: "center"}}><b>Sub Header</b></Box>
            <Grid container spacing={3} style={{padding:"10px 0px 20px 0px"}}>
                <Grid item xs={4}>
                    <TextField id="outlined-basic" label="field16" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                    <TextField id="outlined-basic" label="field17" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                    <TextField id="outlined-basic" label="field18" variant="outlined"/>
                </Grid>
            </Grid>
            <Grid container spacing={3} style={{padding:"10px 0px 20px 0px"}}>
                <Grid item xs={4}>
                    <TextField id="outlined-basic" label="field19" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                    <TextField id="outlined-basic" label="field20" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                    <TextField id="outlined-basic" label="field21" variant="outlined"/>
                </Grid>
            </Grid>
            <Grid container spacing={3} style={{padding:"10px 0px 20px 0px"}}>
                <Grid item xs={4}>
                    <TextField id="outlined-basic" label="field22" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                    <TextField id="outlined-basic" label="field23" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                    <TextField id="outlined-basic" label="field24" variant="outlined"/>
                </Grid>
            </Grid>
            <Grid container spacing={3} style={{padding:"10px 0px 20px 0px"}}>
                <Grid item xs={4}>
                    <TextField id="outlined-basic" label="field25" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                    <TextField id="outlined-basic" label="field26" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                    <TextField id="outlined-basic" label="field27" variant="outlined"/>
                </Grid>
            </Grid>
            <Grid container spacing={3} style={{padding:"10px 0px 20px 0px"}}>
                <Grid item xs={5}>
                    <Paper style={{padding:"20px"}}>
                        <Box p={1}>Box Header Page2</Box>
                        <Box p={1}>Box Sub Header</Box>
                        <Box p={1}>Box Content1</Box>
                        <Box p={1}>Box Content2</Box>
                        <Box p={1}>Box Content3</Box>
                        <Box p={1}>Box Content14</Box>
                    </Paper>
                </Grid>
                <Grid item xs={2}>

                </Grid>
                <Grid item xs={5}>
                <Paper style={{padding:"20px"}}>
                    <Box p={1}>Box Header Page2</Box>
                    <Box p={1}>Box Sub Header</Box>
                    <Box p={1}>Box Content1</Box>
                    <Box p={1}>Box Content2</Box>
                    <Box p={1}>Box Content3</Box>
                    <Box p={1}>Box Content14</Box>
                </Paper>
                </Grid>
            </Grid>
            <Grid container spacing={3} style={{padding:"10px 0px 80px 0px"}}>
                <Grid item xs={4}>
                    <TextField id="outlined-basic" label="field28" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                    <TextField id="outlined-basic" label="field29" variant="outlined"/>
                </Grid>
                <Grid item xs={4}>
                    <TextField id="outlined-basic" label="field30" variant="outlined"/>
                </Grid>
            </Grid>
        </Container>
    )
}

export default Page2