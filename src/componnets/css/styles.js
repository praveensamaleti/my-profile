import { makeStyles } from '@material-ui/core/styles';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBar: {
        // top: 'auto',
        bottom: 0,
      },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        backgroundColor: "#f2f3f",
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
          },
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerContainer: {
        overflow: 'auto',
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        //backgroundColor: theme.palette.background.default,
        padding: theme.spacing(3),
        width: "100%"
    },
    anchor: {
        textDecoration: "none",
        width: "100%",
        textAlign: "center"
    },
    centerText: {
        textAlign: "center"
    },
    paperAnchorLeft: {
        left: "auto"
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    colorPrimary0: {
        backgroundColor: "#e2b6c7 !important"
    },
    barColorPrimary0: {
        backgroundColor: "#ef2e78 !important"
    },
    colorPrimary1: {
        backgroundColor: "#9195ef !important"
    },
    barColorPrimary1: {
        backgroundColor: "#2e38ef !important"
    },
    colorPrimary2: {
        backgroundColor: "#e7ef91 !important"
    },
    barColorPrimary2: {
        backgroundColor: "#a8b517 !important"
    },
    colorPrimary3: {
        backgroundColor: "#e0998c !important"
    },
    barColorPrimary3: {
        backgroundColor: "#e04b2e !important"
    },
    colorPrimary4: {
        backgroundColor: "#91ef92 !important"
    },
    barColorPrimary4: {
        backgroundColor: "#0c690d !important"
    },
    colorPrimary5: {
        backgroundColor: "#90eceb !important"
    },
    barColorPrimary5: {
        backgroundColor: "#0d716f !important"
    },
    colorPrimary6: {
        backgroundColor: "#eaaaec !important"
    },
    barColorPrimary6: {
        backgroundColor: "#95139a !important"
    }
}));

export { useStyles }