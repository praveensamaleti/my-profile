import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { Link } from "react-router-dom";
import { useStyles } from '../css/styles';
import { MenuList } from '../util/Util';
import "./SideBar.css"
import { Hidden, Typography, useTheme } from '@material-ui/core';

export default function SideBar(props) {
  const { window } = props;
  const classes = useStyles();
  const theme = useTheme();
  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <div className="image"></div>
      <Typography variant="h6" gutterBottom className={classes.centerText}>Praveen Samaleti</Typography>
      <Typography variant="caption" display="block" gutterBottom className={classes.centerText}>FullStack Developer</Typography>
      <List>
        {MenuList.map((item, index) => (
          <ListItem button key={index}>
            <Link to={item.pathTo} className={classes.anchor} onClick={props.handleCloseDrawer}>{item.name}</Link>
          </ListItem>
        ))}
      </List>
    </div>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    <>
      <CssBaseline />
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={props.mobileOpen}
            onClose={props.handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
    </>

  );
}
