import React from "react"
import Toolbar from '@material-ui/core/Toolbar';
import {
  Switch,
  Route
} from "react-router-dom";
// import Home from "../home/Home";
import { useStyles } from "../css/styles";
// import Contact from "../contact/Contact";
// import About from "../about/About";
// import Skills from "../../skills/Skills";
import MenuIcon from '@material-ui/icons/Menu';
import { Box, Hidden, IconButton } from "@material-ui/core";
// import Experience from "../Experince/Experience";
import Create from "../NEW/Create";
import Anime from "../css-animation/anime";
import Datef from "../css-animation/datef";


export default function PrivateRouter(props) {
  const classes = useStyles();
  return (
    <main className={classes.content}>
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          edge="start"
          onClick={props.handleDrawerToggle}
          className={classes.menuButton}
        >
          <MenuIcon />
        </IconButton>
      </Toolbar>

      <Box display="flex" flexDirection="row" p={1} m={1}>
        <Box p={1}>
          <Hidden xsDown implementation="css">
            <div style={{ minWidth: 240 }}></div>
          </Hidden>
        </Box>
        <Box p={1} style={{minWidth: "80%"}}>
          <Switch>
            {/* <Route path="/home"><Home /></Route> */}
            {/* <Route path="/about"><About /></Route> */}
            {/* <Route path="/contact"><Contact /></Route> */}
            {/* <Route path="/skills"><Skills /></Route> */}
            {/* <Route path="/experience"><Experience/></Route> */}
            <Route path="/anime"><Anime/></Route>
            <Route path="/date"><Datef/></Route>
            <Route path="/"><Create/></Route>
            
            {/* <Route path="/"><Home /></Route> */}
          </Switch>
        </Box>
      </Box>

    </main>
  )
}