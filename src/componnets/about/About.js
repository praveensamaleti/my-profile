import { Typography } from "@material-ui/core"
import React from "react"

export default function About() {
    return (
        <>
            <Typography variant="h6" gutterBottom >About Me</Typography>
            <Typography>
                Hi I am <b> Praveen Kumar Samaleti </b>, Full stack Developer having 7+ years total experience.
            I did my graduation from Jawaharlal Nehru Technological University Hyderabad in year 2013,
            then I joined accenture services private limited as a full time employee, so there i worked
            on java backend technologies , mainly Core java, J2EE, RESTful web services, JPA and Hibenrate.
            I worked there till year 2017. And then I joined Tata Consultancy Services as IT Analyst.
            Here I have been working as a full stack developer, i work on java, spring boot, apache kafka,
            javascript and reactjs libraries. I also have handon cloud platform like openshift, kubernetes, docker and aws

            </Typography>
        </>
    )
}