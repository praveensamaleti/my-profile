import React from "react";
import './App.css';
import SideBar from './componnets/sidebar/SideBar.js';
import {
  BrowserRouter as Router,
} from "react-router-dom";
import PrivateRouter from './componnets/route/PrivateRouter';
import { Provider } from "react-redux";
import { createStore } from "redux";
import reducer from "./redux/reducer";

const store = createStore(reducer);

function App() {
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const handleCloseDrawer = () => {
    setMobileOpen(false);
  }

  return (
    <Provider store={store}>
      <Router>
        <SideBar handleDrawerToggle={handleDrawerToggle} handleCloseDrawer={handleCloseDrawer} mobileOpen={mobileOpen} />
        <PrivateRouter handleDrawerToggle={handleDrawerToggle} />
      </Router>
    </Provider>

  );
}

export default App;
