
import { P1_VALIDATED, UPDATE_PAGE1, UPDATE_STEP } from "./action-type"

function updateStep(step) {
    return {
        type: UPDATE_STEP,
        payload: step
    }
}

function p1validated(validated) {
    return {
        type: P1_VALIDATED,
        payload: validated
    }
}

function updatePage1(payload) {
    return {
        type: UPDATE_PAGE1,
        payload
    }
}
export { updateStep, p1validated, updatePage1 }