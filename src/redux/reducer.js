import { P1_VALIDATED, UPDATE_PAGE1, UPDATE_STEP } from './action-type';

function reducer(state={currentStep:"Page1"},action) {
    switch (action.type) {
        case UPDATE_STEP:
            return {
                ...state,
                currentStep: action.payload
            };
        case P1_VALIDATED:
            console.log("P1_Validated",action)
            return {
                ...state,
                p1Validated: action.payload
            }
        case UPDATE_PAGE1:
            console.log("UPDATE_PAGE1",action)
            return { ...state, page1Values: {...state.page1Values,...action.payload} }
        default:
            return {};
    }
}

export default reducer;